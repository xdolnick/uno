package cz.mendelu.pjj.xdolnick.projekt.uno;

import org.junit.Test;
import static org.junit.Assert.*;

public class PlayerTest {

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void checkFaceTrue() {
        Player player = new Player("Player1");
        Cards.setActive(true);
        assertEquals(true, player.checkFace());
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void checkFaceFalse() {
        Player player = new Player("Player2");
        Cards.setActive(false);
        assertEquals(false, player.checkFace());
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initHandNotEmpty() {
        Player player = new Player("Player3");
        player.initHand();
        assertFalse(player.getHand().size() == 0);

    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initHandTest() {
        Player player = new Player("Player3");
        player.initHand();
        assertTrue(Cards.getCards().containsAll(player.getHand()));
    }
}