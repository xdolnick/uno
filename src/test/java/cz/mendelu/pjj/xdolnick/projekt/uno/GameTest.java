package cz.mendelu.pjj.xdolnick.projekt.uno;

import org.junit.Test;
import static org.junit.Assert.*;

public class GameTest {
    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initGamePlayersAtLeastTwo() {
        Game game = new Game();
        game.initGame();
        assertTrue(game.getPlayers().size() > 1);
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initGamePlayersHandTest() {
        Game game = new Game();
        game.initGame();
        for (Player player:game.getPlayers()) {
            assertNotNull(player.getHand());
            assertTrue(player.getHand().size() == 7);
        }
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initGameFaceNotNull() {
        Game game = new Game();
        game.initGame();
        assertNotNull(Cards.getFace());
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initGameFaceNotActive() {
        Game game = new Game();
        game.initGame();
        assertFalse(Cards.isActive());
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void initGameFace() {
        Game game = new Game();
        game.initGame();
        assertTrue(Cards.getCards().contains(Cards.getFace()));
    }

}