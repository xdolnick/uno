package cz.mendelu.pjj.xdolnick.projekt.uno;

import org.junit.Test;
import static org.junit.Assert.*;

public class CardsTest {

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void generateCardNotNull() {
        //when
        Card card = Cards.generateCard();
        //then
        assertNotNull(card);
    }

    /**
     * @author xdolnick
     * @version etapa 2
     */
    @Test
    public void generateCard() {
        Card card = Cards.generateCard();
        assertTrue(Cards.getCards().contains(card));
    }
}