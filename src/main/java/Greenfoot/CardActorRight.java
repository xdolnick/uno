package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class CardActorRight extends Actor {
    public CardActorRight() {
        this.update();
    }

    public void update() {
        String string = "images/cards/Uno_flipped_right.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}
