package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class CardActorLeft extends Actor {
    public CardActorLeft() {
        this.update();
    }

    public void update() {
        String string = "images/cards/Uno_flipped_left.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}
