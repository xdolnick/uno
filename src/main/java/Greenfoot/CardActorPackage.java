package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class CardActorPackage extends Actor {
    public CardActorPackage() {
        this.update();
    }

    public void update() {
        String string = "images/cards/Uno.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}