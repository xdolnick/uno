package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class UnoActor extends Actor {
    public UnoActor() {
        this.update();
    }

    public void update() {
        String string = "text.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}
