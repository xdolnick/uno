package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class CardActorTop extends Actor {
    public CardActorTop() {
        this.update();
    }

    public void update() {
        String string = "images/cards/Uno_top.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}