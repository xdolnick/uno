package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class ShowEnd extends Actor {
    public ShowEnd() {
        this.update();
    }

    public void update() {
        String string = "the-end.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}
