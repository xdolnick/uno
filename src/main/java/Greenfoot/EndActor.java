package Greenfoot;

import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;


public class EndActor extends Actor {
    private GameWorld world;

    public EndActor(GameWorld world) {
        this.world = world;
        this.update();
    }

    public void update() {
        String string = "images/quit.png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }

    @Override
    public void act() {
        if (Greenfoot.mouseClicked(this)) {
            System.out.println("You stopped the game");
            world.removeObjects(world.getObjects(CardActor.class));
            world.removeObjects(world.getObjects(CardActorLeft.class));
            world.removeObjects(world.getObjects(CardActorTop.class));
            world.removeObjects(world.getObjects(CardActorRight.class));
            world.removeObjects(world.getObjects(FaceCard.class));
            world.removeObjects(world.getObjects(CardActorPackage.class));
            world.removeObjects(world.getObjects(EndActor.class));
            world.addObject(new ShowEnd(), 550, 350);
            Greenfoot.stop();
        }
    }
}