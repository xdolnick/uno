package Greenfoot;

import cz.mendelu.pjj.xdolnick.projekt.uno.*;
import greenfoot.World;

import java.io.FileWriter;
import java.io.IOException;

/**
 * GUI
 * @author xdolnick
 * @version etapa 4
 */
public class GameWorld extends World{
    private final Game game = new Game();
    boolean winner = false;

    public GameWorld(){
        super(1100,720,1);
        setBackground("images/background.jpg");
        updateMyCards();
        updateOpponentCards();
        updateFaceCard();
    }

    /**
     * Saving in file - winner (0 cards in hand)
     * @author xdolnick
     * @version etapa 4
     */
    public void saveInFile() {
        int index = 0;
        for(Player p : game.getPlayers()){
            if (p.getHand().size() == 0 && index == 0 && !winner) {
                index = 1;
                try {
                    FileWriter myWriter = new FileWriter("score.txt");
                    myWriter.write("Score:\n--------\nWinner is: " + p.getName() + "\n--------");
                    myWriter.close();
                    System.out.println("Successfully saved txt file.");
                    winner = true;
                } catch (IOException e) {
                    System.out.println("An error with writing into the file.");
                    e.printStackTrace();
                }
            }
        }
    }
    public void updateMyCards(){
        int count = 0;
            for (Card c : game.getPlayers().get(0).getHand()) {
                addObject(new CardActor(c.toString(), game,this, game.getPlayers().get(0)), 400 + (count*50), 600);
                count++;
        }
    }
    /**
     * AI implemented
     * @author xdolnick
     * @version etapa 5
     */
    public void updateOpponentCards(){
        int index = 0;
        for (Player p:game.getPlayers()) {
            int count = 0;
            for (Card c : p.getHand()) {
                if (index == 1) {
                    addObject(new CardActorLeft(), 200, 200 + (count * 50));
                }
                if (index == 2) {
                    addObject(new CardActorTop(), 400 + (count * 50), 100);
                }
                if (index == 3) {
                    addObject(new CardActorRight(), 900, 200 + (count * 50));
                }
                count++;
            }
            index++;
        }
    }

    public void checkUno(int i){
        if (i == 0 && game.getPlayers().get(0).getHand().size() == 1) {
            addObject(new UnoActor(), 500,500);
        }
        if (i == 1 && game.getPlayers().get(1).getHand().size() == 1) {
            addObject(new UnoActor(), 100,100);
        }
        if (i == 2 && game.getPlayers().get(2).getHand().size() == 1) {
            addObject(new UnoActor(),500,200);
        }
        if (i == 3 && game.getPlayers().get(3).getHand().size() == 1) {
            addObject(new UnoActor(),1000,100);
        }
    }

    public void updateFaceCard(){
        addObject(new CardActorPackage(), 450, 350);
        addObject(new FaceCard(Cards.getFace().toString()), 600, 350);
        addObject(new EndActor(this), 1000, 700);
    }
}
