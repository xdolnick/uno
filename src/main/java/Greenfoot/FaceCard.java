package Greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;

public class FaceCard extends Actor {
    public FaceCard(String card) {
        this.update(card);
    }

    public void update(String card) {
        String string = "images/cards/"+ card + ".png";
        GreenfootImage image = new GreenfootImage(string);
        setImage(image);
    }
}