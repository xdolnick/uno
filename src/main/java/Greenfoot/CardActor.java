package Greenfoot;

import cz.mendelu.pjj.xdolnick.projekt.uno.*;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;

import java.util.Arrays;
import java.util.List;

public class CardActor extends Actor {
    private String card;
    private Game game;
    private GameWorld world;
    private Player player;
    private boolean played = false;

    public CardActor(String card, Game game, GameWorld world, Player player) {
        this.card = card;
        this.game = game;
        this.world = world;
        this.player = player;
        this.update(card);
    }
    public void update(String card) {
        GreenfootImage image=null;
        String string = "images/cards/" + card + ".png";
        image = new GreenfootImage(string);
        setImage(image);
    }
    /**
     * AI implemented
     * @author xdolnick
     * @version etapa 5
     */
    private void otherPlayers(){
        world.checkUno(0);
        Greenfoot.delay(100);
        world.removeObjects(world.getObjects(UnoActor.class));
        game.getPlayers().get(1).round();
        world.removeObjects(world.getObjects(CardActorLeft.class));
        world.updateOpponentCards();
        world.updateFaceCard();
        world.checkUno(1);
        Greenfoot.delay(100);
        world.removeObjects(world.getObjects(UnoActor.class));

        game.getPlayers().get(2).round();
        world.removeObjects(world.getObjects(CardActorTop.class));
        world.updateOpponentCards();
        world.updateFaceCard();
        world.checkUno(2);
        Greenfoot.delay(100);
        world.removeObjects(world.getObjects(UnoActor.class));


        game.getPlayers().get(3).round();
        world.removeObjects(world.getObjects(CardActorRight.class));
        world.updateOpponentCards();
        world.updateFaceCard();
        world.checkUno(3);
        Greenfoot.delay(100);
        world.removeObjects(world.getObjects(UnoActor.class));
        world.saveInFile();

    }
    @Override
    public void act() {
        played = player.getHand().size() == 0;

        if (!player.canPlay() && !Cards.isActive()){
            if (player.getHand().size() > 0) {
                player.getHand().add(Cards.generateCard());

                world.removeObjects(world.getObjects(this.getClass()));
                world.updateMyCards();
            }
            played = true;
        }
        if (Cards.isActive()) {
            if(Cards.getFace().getClass().getSimpleName().equals("PlusCard")){
                for (int i = 0; i < ((PlusCard)Cards.getFace()).getNumber(); i++){
                    if(player.getHand().size() < 10) {
                        player.getHand().add(Cards.generateCard());
                    }
                }

                world.removeObjects(world.getObjects(this.getClass()));
                world.updateMyCards();
            }
            Cards.setActive(false);
            played = true;
        }
        if (Greenfoot.mouseClicked(this)) {
            if (!Cards.checkFaceNumber(parse(card)) && !parse(card).getColor().equals(Cards.getFace().getColor())
                    && !Cards.getFace().getClass().getSimpleName().equals((card.split("_"))[0]) ){
                if (!parse(card).getColor().equals("black")) {
                    return;
                }
            }

            if (!played) {
                if (parse(card).getClass().getSimpleName().equals("PlusCard") ||
                        parse(card).getClass().getSimpleName().equals("StopCard")) Cards.setActive(true);
                Cards.setFace(parse(card));
                if (parse(card).getClass().getSimpleName().equals("PlusCard") &&
                        ((PlusCard) parse(card)).getNumber() == 4) {
                    Card temp = Cards.getFace();
                    temp.setColor(player.maxColor());
                    Cards.setFace(temp);
                }
                if (parse(card).getClass().getSimpleName().equals("ChangeCard")) {
                    Card temp = Cards.getFace();
                    temp.setColor(player.maxColor());
                    Cards.setFace(temp);
                }

                world.addObject(new CardActorPackage(), 450, 350);
                world.addObject(new FaceCard(Cards.getFace().toString()), 600, 350);
                player.getHand().remove(parse(card));
                world.updateFaceCard();
                world.removeObjects(world.getObjects(this.getClass()));
                world.updateMyCards();
            }
            otherPlayers();
        }
        if (played){
            otherPlayers();
        }
    }

    private Card parse(String s){
        List list = Arrays.asList(s.split("_").clone());
        switch (list.get(0).toString()) {
            case "PlusCard":
                if (list.get(1).toString().equals("black")) {
                    return new PlusCard(4, list.get(1).toString());
                }
                return new PlusCard(2,list.get(1).toString());
            case "ChangeCard":
                return new ChangeCard(list.get(1).toString());
            case "StopCard":
                return new StopCard(list.get(1).toString());
            default:
                return new BasicCard(Integer.parseInt(list.get(1).toString()),list.get(2).toString());
        }
    }

}