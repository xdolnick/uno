package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.Objects;

public abstract class Card {
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    /**
     * equals
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(color, card.color);
    }
    /**
     * hashcode
     * @author xdolnick
     * @version etapa 3
     */

    @Override
    public int hashCode() {
        return Objects.hash(color);
    }
}
