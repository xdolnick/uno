package cz.mendelu.pjj.xdolnick.projekt.uno;

public class PlayerMe extends Player{
    public PlayerMe(String name) {
        super(name);
    }

    @Override
    public void round(){
        setClicked(false);
        if (checkFace()){
            if (Cards.getFace().getClass().getSimpleName().equals("PlusCard")){
                for(int i = 0; i<((PlusCard)(Cards.getFace())).getNumber(); i++){
                    getHand().add(Cards.generateCard());
                }
            }
            Cards.setActive(false);
            return;
        }
        if (!canPlay()){
            getHand().add(Cards.generateCard());
        }
    }
}
