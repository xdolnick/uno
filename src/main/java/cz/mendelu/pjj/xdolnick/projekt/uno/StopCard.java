package cz.mendelu.pjj.xdolnick.projekt.uno;

public class StopCard extends Card{
    public StopCard(String color) {
        setColor(color);
    }

    @Override
    public String toString() {
        return "StopCard_" + getColor();
    }
}
