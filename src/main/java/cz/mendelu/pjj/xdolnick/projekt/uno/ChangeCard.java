package cz.mendelu.pjj.xdolnick.projekt.uno;

public class ChangeCard extends Card{
    public ChangeCard(String color) {
        setColor(color);
    }

    @Override
    public String toString() {
        return "ChangeCard_" + getColor();
    }
}
