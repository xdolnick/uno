package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.Objects;

public class PlusCard extends Card{
    private int number;

    public PlusCard(int number, String color) {
        this.number = number;
        this.setColor(color);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlusCard plusCard = (PlusCard) o;
        return number == plusCard.number && getColor().equals(plusCard.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(number,getColor());
    }

    @Override
    public String toString() {
        return "PlusCard_" + getColor() + "_" + getNumber();
    }
}
