package cz.mendelu.pjj.xdolnick.projekt.uno;

public class PlayerAI extends Player{
    public PlayerAI(String name) {
        super(name);
    }

    @Override
    public void round(){
        if (getHand().size() == 0) return;
        if (checkFace()){
            if (Cards.getFace().getClass().getSimpleName().equals("PlusCard")){
                for(int i = 0; i<((PlusCard)(Cards.getFace())).getNumber(); i++){
                    if (getHand().size() < 10) {
                        getHand().add(Cards.generateCard());
                    }
                }
            }
            Cards.setActive(false);
            return;
        }
        if (!canPlay()){
            if (getHand().size() < 10) {
                getHand().add(Cards.generateCard());
            }
            return;
        }
        Card temp = null;
        for (Card c: getHand()) {
            if (Cards.hasFaceColor(c) || Cards.hasFaceNumber(c) || c.getColor().equals("black")){
                Cards.setFace(c);
                temp = c;
                break;
            }
        }

        if (temp != null) {
            if (temp.getClass().getSimpleName().equals("ChangeCard")) {
                Cards.setFace(new ChangeCard(maxColor()));
            }
            if (temp.getClass().getSimpleName().equals("PlusCard") && ((PlusCard)temp).getNumber() == 4) {
                Cards.setFace(new PlusCard(4,maxColor()));
            }
            if (temp.getClass().getSimpleName().equals("StopCard") ||
                    temp.getClass().getSimpleName().equals("PlusCard")) Cards.setActive(true);
            removeCard(temp);
        }
    }
}
