package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.*;

public class Cards {
    private static Set<Card> cards = new HashSet<>(){{
        add(new BasicCard(0,"red"));
        add(new BasicCard(1,"red"));
        add(new BasicCard(2,"red"));
        add(new BasicCard(3,"red"));
        add(new BasicCard(4,"red"));
        add(new BasicCard(5,"red"));
        add(new BasicCard(6,"red"));
        add(new BasicCard(7,"red"));
        add(new BasicCard(8,"red"));
        add(new BasicCard(9,"red"));
        add(new BasicCard(0,"blue"));
        add(new BasicCard(1,"blue"));
        add(new BasicCard(2,"blue"));
        add(new BasicCard(3,"blue"));
        add(new BasicCard(4,"blue"));
        add(new BasicCard(5,"blue"));
        add(new BasicCard(6,"blue"));
        add(new BasicCard(7,"blue"));
        add(new BasicCard(8,"blue"));
        add(new BasicCard(9,"blue"));
        add(new BasicCard(0,"green"));
        add(new BasicCard(1,"green"));
        add(new BasicCard(2,"green"));
        add(new BasicCard(3,"green"));
        add(new BasicCard(4,"green"));
        add(new BasicCard(5,"green"));
        add(new BasicCard(6,"green"));
        add(new BasicCard(7,"green"));
        add(new BasicCard(8,"green"));
        add(new BasicCard(9,"green"));
        add(new BasicCard(0,"yellow"));
        add(new BasicCard(1,"yellow"));
        add(new BasicCard(2,"yellow"));
        add(new BasicCard(3,"yellow"));
        add(new BasicCard(4,"yellow"));
        add(new BasicCard(5,"yellow"));
        add(new BasicCard(6,"yellow"));
        add(new BasicCard(7,"yellow"));
        add(new BasicCard(8,"yellow"));
        add(new BasicCard(9,"yellow"));
        add(new StopCard("red"));
        add(new StopCard("blue"));
        add(new StopCard("green"));
        add(new StopCard("yellow"));
        add(new PlusCard(2,"red"));
        add(new PlusCard(2,"blue"));
        add(new PlusCard(2,"green"));
        add(new PlusCard(2,"yellow"));
        add(new PlusCard(4,"black"));
        add(new ChangeCard("black"));
    }};
    private static final Map<String, Integer> probability;

    static {
        probability = new HashMap<String, Integer>() {{
            put("BasicCard", 2 );
            put("ChangeCard", 1);
            put("StopCard", 1);
            put("PlusCard", 1);
        }};
    }

    private static boolean active;
    private static Card face;
    /**
     * Randomly chooses one card from list of cards
     * @return Randomly selected Card
     * @author xdolnick
     * @version etapa 3
     */
    public static Card generateCard(){
        Card card = generate();
        assert card != null; // never happens
        String name = card.getClass().getSimpleName();
        int prob = probability.get(name);
        if (prob == 1){
            int number = new Random().nextInt(2);
            if (number == 0) generateCard();
        }
        return card;
    }
    /**
     * generates random number
     * @author xdolnick
     * @version etapa 3
     */
    private static Card generate(){
        int index = new Random().nextInt(cards.size());  //generate random int -> (0 - cards.size)
        int i = 0;
        for(Card c: cards) {
            if (i == index) {
                return c;
            }
            i++;
        }
        return null; // never happens
    }
    /**
     * generates face card
     * @author xdolnick
     * @version etapa 3
     */
    public static Card generateFace(){
        Card card = generate();
        assert card != null;
        if (!card.getClass().getSimpleName().equals("BasicCard")){
            return generateFace();
        }
        return card;
    }

    public static boolean isActive() {
        return active;
    }

    public static Set<Card> getCards() {
        return cards;
    }

    public static void setActive(boolean active) {
        Cards.active = active;
    }

    public static Card getFace() {
        return face;
    }

    public static void setFace(Card face) {
        Cards.face = face;
    }

    public static boolean hasFaceNumber(Card c){
        if (Cards.getFace().getClass().getSimpleName().equals("StopCard") ||
                Cards.getFace().getClass().getSimpleName().equals("ChangeCard")) return false;

        if (Cards.getFace().getClass().getSimpleName().equals("PlusCard")){
            if (c.getClass().getSimpleName().equals("PlusCard")){
                if (((PlusCard)c).getNumber() == ((PlusCard)Cards.getFace()).getNumber()) return true;
            }
        }

        if (Cards.getFace().getClass().getSimpleName().equals("BasicCard")) {
            if (c.getClass().getSimpleName().equals("BasicCard")) {
                return ((BasicCard) c).getNumber() == ((BasicCard) Cards.getFace()).getNumber();
            }
        }
        return false;
    }
    public static boolean hasFaceColor(Card c) {
        return c.getColor().equals(Cards.getFace().getColor());
    }

    public static boolean checkFaceNumber(Card card){
        if (Cards.getFace().getClass().getSimpleName().equals("StopCard") ||
                Cards.getFace().getClass().getSimpleName().equals("ChangeCard")) return false;
        if (Cards.getFace().getClass().getSimpleName().equals("PlusCard")
                && card.getClass().getSimpleName().equals("PlusCard") ){
            if (((PlusCard)card).getNumber() == ((PlusCard)Cards.getFace()).getNumber()) return true;
        }
        if (Cards.getFace().getClass().getSimpleName().equals("BasicCard")
                && card.getClass().getSimpleName().equals("BasicCard") ) {
            return ((BasicCard) card).getNumber() == ((BasicCard) Cards.getFace()).getNumber();
        }
        return false;
    }
}
