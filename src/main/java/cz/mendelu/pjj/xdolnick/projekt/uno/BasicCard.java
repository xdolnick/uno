package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.Objects;

public class BasicCard extends Card{
    private int number;

    public int getNumber() {
        return number;
    }

    public BasicCard(int number,String color) {
        this.number = number;
        this.setColor(color);
    }
    /**
     * equals
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicCard basicCard = (BasicCard) o;
        return number == basicCard.number && getColor().equals(basicCard.getColor());
    }
    /**
     * hashcode
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public int hashCode() {
        return Objects.hash(number, getColor());
    }

    @Override
    public String toString() {
        return "BasicCard_" + number + "_" + getColor();
    }
}
