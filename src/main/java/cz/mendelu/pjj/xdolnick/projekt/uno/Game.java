package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private List<Player> players = new ArrayList<>();

    public Game() {
        initGame();
    }
    /**
     * Initializes the game. It gives each player cards and sets face card.
     * @author xdolnick
     * @version etapa 3
     */
    public void initGame(){
        players.add(new PlayerMe("Me"));
        players.add(new PlayerAI("Robino"));
        players.add(new PlayerAI("Andrej"));
        players.add(new PlayerAI("Monika"));
        for(Player p: players){
            p.initHand();
        }
        Cards.setFace(Cards.generateFace());
    }

    public List<Player> getPlayers() {
        return players;
    }

}
