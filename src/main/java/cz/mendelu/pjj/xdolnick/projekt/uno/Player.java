package cz.mendelu.pjj.xdolnick.projekt.uno;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Player {
    private boolean clicked = false;
    private String name;
    private List<Card> hand = new ArrayList<>();
    private boolean uno = false;

    public Player(String name) {
        this.name = name;
    }

    /**
     * Checks if face card (currently at the top) is active (StopCard or PlusCard)
     * @return true - face is active, false - face is not active
     * @author xdolnick
     * @version etapa 3
     */
    public boolean checkFace(){
        return Cards.isActive();
    }

    /**
     * Generates 7 cards for player
     * @author xdolnick
     * @version etapa 3
     */
    public void initHand(){
        for (int i = 0; i < 7; i++){
            hand.add(Cards.generateCard());
        }
    }

    public List<Card> getHand() {
        return hand;
    }

    public boolean isUno() {
        return uno;
    }

    public void setUno(boolean uno) {
        this.uno = uno;
    }

    public String getName() {
        return name;
    }
    /**
     * round with active face card
     * @author xdolnick
     * @version etapa 3
     */
    public void round(){
        System.out.println("should never happen");
    }
    /**
     * checking if player can play
     * @author xdolnick
     * @version etapa 3
     */
    public boolean canPlay(){
        if(hasBlackCard()) return true;
        if(hasFaceNumber()) return true;
        if(hasFaceColor()) return true;
        if( Cards.getFace().getClass().getSimpleName().equals("StopCard") && hasStopCard()) return true;
        return false;
    }
    protected void removeCard(Card card){
        hand.remove(card);
    }
    private boolean hasBlackCard(){
        for (Card c: hand) {
            if (c.getColor().equals("black")) return true;
        }
        return false;
    }

    private boolean hasStopCard(){
        for (Card c: hand) {
            if (c.getClass().getSimpleName().equals("StopCard")) return true;
        }
        return false;
    }

    public String maxColor(){
        int yellow = 0;
        int red = 0;
        int green = 0;
        int blue = 0;
        for (Card c : getHand()) {
            String x = c.getColor();
            if (x.equals("yellow")){
                yellow++;
            }
            if (x.equals("red")){
                red++;
            }
            if (x.equals("blue")){
                blue++;
            }
            if (x.equals("green")){
                green++;
            }

        }
        int result = Math.max(Math.max(yellow,red),Math.max(green,blue));
        if (yellow == result) return "yellow";
        if (red == result) return "red";
        if (green == result) return "green";
        return "blue";
    }

    protected boolean hasFaceNumber(){
        if (Cards.getFace().getClass().getSimpleName().equals("StopCard") ||
                Cards.getFace().getClass().getSimpleName().equals("ChangeCard")) return false;

        if (Cards.getFace().getClass().getSimpleName().equals("PlusCard")){
            for (Card c: hand) {
                if (c.getClass().getSimpleName().equals("PlusCard")){
                    if (((PlusCard)c).getNumber() == ((PlusCard)Cards.getFace()).getNumber()) return true;
                }

            }
        }
        if (Cards.getFace().getClass().getSimpleName().equals("BasicCard")) {
            for (Card c : hand) {
                if (c.getClass().getSimpleName().equals("BasicCard")) {
                    if (((BasicCard)c).getNumber() == ((BasicCard)Cards.getFace()).getNumber()) return true;
                }
            }
        }

        return false;

    }
    protected boolean hasFaceColor(){
        for (Card c: hand) {
            if (c.getColor().equals(Cards.getFace().getColor())) return true;
        }
        return false;
    }

    /**
     * equals
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return uno == player.uno &&
                Objects.equals(name, player.name) &&
                Objects.equals(hand, player.hand);
    }
    /**
     * hashcode
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, hand, uno);
    }
    /**
     * toString
     * @author xdolnick
     * @version etapa 3
     */
    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", hand=" + hand +
                ", uno=" + uno +
                '}';
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }
}
