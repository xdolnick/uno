import bh.greenfoot.runner.GreenfootRunner;
import Greenfoot.GameWorld;

public class Runner extends GreenfootRunner {

    static{
        bootstrap(Runner.class,
                Configuration.forWorld(GameWorld.class)
                        .projectName("UNO")
                        .hideControls(true));
    }

    public static void main(String[] args) {

        GreenfootRunner.main(args);
    }
}
